package com.optimind.optimindexam.listener;

import android.view.View;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public interface OnItemClickListener {
    void onItemClick(View view, int position, long id);
}
