package com.optimind.optimindexam.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public class Preferences {

    private static SharedPreferences settings;
    private static SharedPreferences.Editor editor;
    private static String PREF_FILENAME = "OptimindPreferences";

    private static String KEY_IS_LOGGED_IN = "isloggedin";
    private static String KEY_EMAIL_ADDRESS = "emailAddress";

    public static void Load(Context context) {
        settings = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        editor = settings.edit();
    }

    public static void setUserIsLoggedIn(Context context, boolean isloggedin) {
        if (settings == null) {
            Load(context);
        }
        editor.putBoolean(KEY_IS_LOGGED_IN, isloggedin);
        editor.commit();
    }

    public static boolean isUserLoggedIn(Context context) {
        if (settings == null) {
            Load(context);
        }
        return settings.getBoolean(KEY_IS_LOGGED_IN, false);
    }
}
