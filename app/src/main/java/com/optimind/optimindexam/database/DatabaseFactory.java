package com.optimind.optimindexam.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.optimind.optimindexam.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public class DatabaseFactory extends SQLiteOpenHelper {

    private static final String TAG = DatabaseFactory.class.getSimpleName();

    public static String DATABASE_NAME = "OptimindExamDB";
    public static int DATABASE_VERSION = 1;

    public static String USER_TABLE = "user";
    public static String USER_KEY_FIRSTNAME = "firstname";
    public static String USER_KEY_LASTNAME = "lastname";
    public static String USER_KEY_EMAIL_ADDRESS = "emailaddress";
    public static String USER_KEY_ADDRESS = "address";
    public static String USER_KEY_AGE = "age";
    private String[] USER_COLUMNS = {USER_KEY_FIRSTNAME, USER_KEY_LASTNAME, USER_KEY_EMAIL_ADDRESS,
            USER_KEY_ADDRESS, USER_KEY_AGE};

    private static DatabaseFactory instance;
    private SQLiteDatabase SQLDB;

    public DatabaseFactory(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseFactory getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseFactory(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            dropAllTable(db);
        }
    }

    public SQLiteDatabase getDatabaseInstance() {
        if (SQLDB == null) {
            SQLDB = this.getWritableDatabase();
        }
        return SQLDB;
    }

    public synchronized void openDatabase() {
        if (SQLDB == null) {
            getDatabaseInstance();
        }
    }

    public synchronized void closeDatabase() {
        if (SQLDB.isOpen()) {
            SQLDB.close();
            close();
        }
    }

    public synchronized void dropAllTable(SQLiteDatabase db) {

        for (String table : getTableNames()) {
            String statement = "DROP TABLE IF EXISTS " + table + " ";
            db.execSQL(statement);
        }
    }

    public List<String> getTableNames() {
        List<String> list = new ArrayList<>();
        list.add(USER_TABLE);
        return list;
    }

    public void createAllTable() {
        List<String> list;
        list = getTableNames();

        for (String tablename : list) {
            if (!isTableExists(tablename)) {
                createTable(tablename);
            }
        }
    }

    public boolean isTableExists(String name) {
        openDatabase();
        Cursor cursor;
        cursor = SQLDB.rawQuery("SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name = '"
                + name + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public synchronized void createTable(String tableName) {
        openDatabase();
        Log.i(TAG, "Creating table " + tableName);
        String SQL_STATEMENT = "";
        if (tableName.equalsIgnoreCase(USER_TABLE)) {
            SQL_STATEMENT = "CREATE TABLE " + tableName + " ("
                    + USER_KEY_FIRSTNAME + " TEXT, "
                    + USER_KEY_LASTNAME + " TEXT, "
                    + USER_KEY_EMAIL_ADDRESS + " TEXT, "
                    + USER_KEY_ADDRESS + " TEXT, "
                    + USER_KEY_AGE + " TEXT"
                    + ")";
        }

        SQLDB.execSQL(SQL_STATEMENT);
    }

    public synchronized long insert(final Object object) {
        openDatabase();
        ContentValues cv = new ContentValues();

        if (object instanceof User) {
            cv.put(USER_KEY_FIRSTNAME, ((User) object).getFirstName());
            cv.put(USER_KEY_LASTNAME, ((User) object).getLastName());
            cv.put(USER_KEY_EMAIL_ADDRESS, ((User) object).getEmailAddress());
            cv.put(USER_KEY_ADDRESS, ((User) object).getAddress());
            cv.put(USER_KEY_AGE, ((User) object).getAge());

            return SQLDB.insert(USER_TABLE, null, cv);
        }
        return -1;
    }

    public synchronized long update(Object object) {
        ContentValues cv = new ContentValues();
        if (object instanceof User) {
            cv.put(USER_KEY_FIRSTNAME, ((User) object).getFirstName());
            cv.put(USER_KEY_LASTNAME, ((User) object).getLastName());
            cv.put(USER_KEY_EMAIL_ADDRESS, ((User) object).getEmailAddress());
            cv.put(USER_KEY_ADDRESS, ((User) object).getAddress());
            cv.put(USER_KEY_AGE, ((User) object).getAge());

            String where = USER_KEY_EMAIL_ADDRESS + " = ?";
            String[] whereArgs = {String.valueOf(((User) object).getEmailAddress())};

            return SQLDB.update(USER_TABLE, cv, where, whereArgs);

        }

        return -1;
    }

    public synchronized long insertOrUpdate(Object object) {
        openDatabase();
        if (!checkIfExists(object)) {
            return insert(object);
        } else {
            return update(object);
        }
    }

    public synchronized boolean checkIfExists(Object object) {
        openDatabase();
        boolean exist = false;
        Cursor cursor = null;
        if (object instanceof User) {
            User user = (User) object;
            String selection = USER_KEY_EMAIL_ADDRESS + " = ?";
            String[] selectionArgs = {String.valueOf(user.getEmailAddress())};
            cursor = SQLDB.query(true, USER_TABLE, USER_COLUMNS, selection, selectionArgs, null,
                    null, null, null);
        }

        if (cursor != null) {
            exist = cursor.getCount() > 0;
            cursor.close();
        }
        return exist;
    }

    private Cursor selectAllDataOnTable(String tablename, String[] columns) {
        openDatabase();
        return SQLDB.query(tablename, columns, null, null, null, null, null);
    }

    public long deleteAllDataOnTable(String tablename) {
        openDatabase();
        return SQLDB.delete(tablename, null, null);
    }

    public List<User> selectAllUsers() {
        openDatabase();
        Cursor cursor = selectAllDataOnTable(USER_TABLE, USER_COLUMNS);
        List<User> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            User user = new User();
            user.setFirstName(cursor.getString(cursor.getColumnIndex(DatabaseFactory.USER_KEY_FIRSTNAME)));
            user.setLastName(cursor.getString(cursor.getColumnIndex(DatabaseFactory.USER_KEY_LASTNAME)));
            user.setEmailAddress(cursor.getString(cursor.getColumnIndex(DatabaseFactory.USER_KEY_EMAIL_ADDRESS)));
            user.setAddress(cursor.getString(cursor.getColumnIndex(DatabaseFactory.USER_KEY_ADDRESS)));
            user.setAge(cursor.getString(cursor.getColumnIndex(DatabaseFactory.USER_KEY_AGE)));
            list.add(user);
        }
        cursor.close();
        return list;
    }
}
