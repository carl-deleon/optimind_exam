package com.optimind.optimindexam.network;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public class Endpoint {

    public static final String BASE_URL = "http://dev.optiminddigital.com/test-mobile/";
    public static final String POST_LOGIN = "login.php";
}
