package com.optimind.optimindexam.network;

import com.optimind.optimindexam.model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public interface RestService {

    @FormUrlEncoded
    @POST(Endpoint.POST_LOGIN)
    Call<LoginResponse> postLogin(@Field("email") String emailAddress, @Field("password") String password);
}
