package com.optimind.optimindexam.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public class User implements Parcelable {

    @SerializedName("firstname")
    String mFirstName;
    @SerializedName("lastname")
    String mLastName;
    @SerializedName("email")
    String mEmailAddress;
    @SerializedName("address")
    String mAddress;
    @SerializedName("age")
    String mAge;

    public User() {
    }

    public User(String firstName, String lastName, String emailAddress, String address, String age) {
        mFirstName = firstName;
        mLastName = lastName;
        mEmailAddress = emailAddress;
        mAddress = address;
        mAge = age;
    }

    public User(Parcel in) {
        mFirstName = in.readString();
        mLastName = in.readString();
        mEmailAddress = in.readString();
        mAddress = in.readString();
        mAge = in.readString();
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getEmailAddress() {
        return mEmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        mEmailAddress = emailAddress;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFirstName);
        dest.writeString(mLastName);
        dest.writeString(mEmailAddress);
        dest.writeString(mAddress);
        dest.writeString(mAge);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
