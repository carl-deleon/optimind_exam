package com.optimind.optimindexam.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public class LoginResponse {

    @SerializedName("status")
    String mStatus;
    @SerializedName("message")
    String mMessage;
    @SerializedName("users")
    List<User> mUsers;

    public List<User> getUsers() {
        return mUsers;
    }

    public void setUsers(List<User> users) {
        mUsers = users;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
}
