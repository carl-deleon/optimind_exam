package com.optimind.optimindexam.app;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public class Constants {

    public static final String LOGIN_STATUS_SUCCESS = "true";
    public static final String LOGIN_STATUS_FAILED = "false";
}
