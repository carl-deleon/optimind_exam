package com.optimind.optimindexam.app;

import android.app.Application;

import com.optimind.optimindexam.network.Endpoint;
import com.optimind.optimindexam.network.RestService;
import com.optimind.optimindexam.util.Preferences;

import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public class AppController extends Application {

    private static AppController sAppController;
    private static RestService sRestService;
    private static Retrofit sRetrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        Preferences.Load(this);

        sAppController = this;

        sRetrofit = new Retrofit.Builder()
                .baseUrl(Endpoint.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        sRestService = sRetrofit.create(RestService.class);
    }

    public static synchronized AppController getAppController() {
        return sAppController;
    }

    public static synchronized RestService getRestService() {
        return sRestService;
    }

    public static synchronized Retrofit getRetrofit() {
        return sRetrofit;
    }
}
