package com.optimind.optimindexam.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.optimind.optimindexam.R;
import com.optimind.optimindexam.listener.OnItemClickListener;
import com.optimind.optimindexam.model.User;

import java.util.List;

/**
 * Created by Carl Justine De leon on 2/12/2016.
 */
public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.UserViewHolder> {

    private Context mContext;
    private List<User> mUsers;

    private OnItemClickListener mListener;

    public UserRecyclerAdapter(Context context, List<User> users, OnItemClickListener listener) {
        mContext = context;
        mUsers = users;
        mListener = listener;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        User user = mUsers.get(position);

        holder.completeName.setText(user.getFirstName() + " " + user.getLastName());
        holder.emailAddress.setText(user.getEmailAddress());

        holder.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, long id) {
                mListener.onItemClick(view, position, id);
            }
        });
    }

    public void updateList(List<User> userList) {
        mUsers = userList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView completeName, emailAddress;
        private OnItemClickListener mListener;

        public void setListener(OnItemClickListener listener) {
            mListener = listener;
        }

        public UserViewHolder(View itemView) {
            super(itemView);

            completeName = (TextView) itemView.findViewById(R.id.complete_name);
            emailAddress = (TextView) itemView.findViewById(R.id.email_address);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(v, getLayoutPosition(), v.getId());
        }
    }
}

