package com.optimind.optimindexam.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.optimind.optimindexam.R;
import com.optimind.optimindexam.model.User;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_KEY_USER = "user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView mFirstName = (TextView) findViewById(R.id.first_name);
        TextView mLastName = (TextView) findViewById(R.id.last_name);
        TextView mEmailAddress = (TextView) findViewById(R.id.email_address);
        TextView mAddress = (TextView) findViewById(R.id.address);
        TextView mAge = (TextView) findViewById(R.id.age);

        if (getIntent().getParcelableExtra(EXTRA_KEY_USER) != null) {
            User mUser = getIntent().getParcelableExtra(EXTRA_KEY_USER);

            mFirstName.setText(String.format(getResources().getString(R.string.format_first_name), mUser.getFirstName()));
            mLastName.setText(String.format(getResources().getString(R.string.format_last_name), mUser.getLastName()));
            mEmailAddress.setText(String.format(getResources().getString(R.string.format_email_address), mUser.getEmailAddress()));
            mAddress.setText(String.format(getResources().getString(R.string.format_address), mUser.getAddress()));
            mAge.setText(String.format(getResources().getString(R.string.format_age), mUser.getAge()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
