package com.optimind.optimindexam.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.optimind.optimindexam.R;
import com.optimind.optimindexam.app.AppController;
import com.optimind.optimindexam.app.Constants;
import com.optimind.optimindexam.database.DatabaseFactory;
import com.optimind.optimindexam.model.LoginResponse;
import com.optimind.optimindexam.model.User;
import com.optimind.optimindexam.util.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputLayout mLayoutEmailAddress, mLayoutPassword;
    private EditText mEmailAddress, mPassword;
    private Button mButtonSignIn;

    private ProgressDialog mProgressDialog;

    private DatabaseFactory DBF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        DBF = new DatabaseFactory(this);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        mEmailAddress = (EditText) findViewById(R.id.input_email_address);
        mPassword = (EditText) findViewById(R.id.input_password);
        mLayoutEmailAddress = (TextInputLayout) findViewById(R.id.input_layout_email_address);
        mLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        mButtonSignIn = (Button) findViewById(R.id.button_sign_in);

        mEmailAddress.addTextChangedListener(new ValidationTextWatcher(mEmailAddress));
        mPassword.addTextChangedListener(new ValidationTextWatcher(mPassword));
        mButtonSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_sign_in:
                signIn();
                break;
        }
    }

    private void signIn() {
        if (!validateEmailAddress()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        if (validateEmailAddress() && validatePassword()) {
            showProgressDialog("Signing in your account. Please wait...");


            Call<LoginResponse> response = AppController.getRestService()
                    .postLogin(mEmailAddress.getText().toString(), mPassword.getText().toString());

            response.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Response<LoginResponse> response) {
                    hideProgressDialog();

                    LoginResponse loginResponse = response.body();

                    if (loginResponse.getStatus().equals(Constants.LOGIN_STATUS_SUCCESS)) {
                        // Save to DB
                        for (User user : loginResponse.getUsers()) {
                            DBF.insertOrUpdate(user);
                        }

                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();

                        // Set user logged in flag = true in preferences
                        Preferences.setUserIsLoggedIn(getApplicationContext(), true);
                    } else {
                        Toast.makeText(getApplicationContext(), loginResponse.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    hideProgressDialog();
                    Toast.makeText(getApplicationContext(), t.getLocalizedMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private class ValidationTextWatcher implements TextWatcher {

        private View view;

        public ValidationTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.input_email_address:
                    validateEmailAddress();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }

    private boolean validatePassword() {
        if (mPassword.getText().toString().trim().isEmpty()) {
            mLayoutPassword.setError(getString(R.string.error_password));
            requestFocus(mPassword);
            return false;
        } else {
            mLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmailAddress() {
        String email = mEmailAddress.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            mLayoutEmailAddress.setError(getString(R.string.error_email_address));
            requestFocus(mEmailAddress);
            return false;
        } else {
            mLayoutEmailAddress.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) &&
                android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);

        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
