package com.optimind.optimindexam.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.optimind.optimindexam.R;
import com.optimind.optimindexam.database.DatabaseFactory;
import com.optimind.optimindexam.util.Preferences;

public class SplashActivity extends AppCompatActivity {

    public static final int SPLASH_DURATION = 1000;
    private DatabaseFactory DBF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        DBF = DatabaseFactory.getInstance(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                DBF.createAllTable();

                if (Preferences.isUserLoggedIn(getApplicationContext())) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }
        }, SPLASH_DURATION);
    }
}
